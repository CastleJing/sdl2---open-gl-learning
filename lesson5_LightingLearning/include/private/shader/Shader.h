#ifndef SHADER_H
#define SHADER_H

#include "IShaderFormFile.h"

class Shader : public gl::IShaderFormFile
{
public:
    Shader() :
        gl::IShaderFormFile("shader/shader.vert",
                            "shader/shader.frag",
                            "Shader")
                            { }
};


#endif //SHADER_H
