#ifndef COMMON_CACHE_H
#define COMMON_CACHE_H

#include <unordered_map>
#include <optional>

// cache small object, do not use it for big object
template <typename T1, typename T2>
class Cache
{
private:
    std::unordered_map<T1, T2> cache;
public:
    Cache() = default;
    std::optional<T2> GetOrNull(const T1& uid)
    {
        auto iter = cache.find(uid);
        if (iter == cache.end())
            return std::nullopt;
        return iter->second;
    }
    void AddOrUpdate(const T1& uid, const T2& o)
    {
        cache[uid] = T2(o);
    }
    [[nodiscard]] size_t Size() const { return cache.size(); }
};

#endif //COMMON_CACHE_H
