#ifndef COMMON_UTIL_H
#define COMMON_UTIL_H

#include <vector>
#include <list>
#include <cstdint>
#include <string>

namespace util
{
    template<typename T>
    [[maybe_unused]] inline void list2vector(const std::list<T>& s, std::vector<T>& t)
    {
        std::vector<T> nt;
        nt.reserve(s.size());
        for (const auto& i: s)
        {
            nt.emplace_back(i);
        }
        t.swap(nt);
    }

    /**
     * @return textureId in OpenGL, 0 for failed.
     */
    uint32_t TextureFromFile(const std::string& path);

    uint32_t TextureFromMemory(int width, int height, int channel, uint8_t* data);

}

#endif //COMMON_UTIL_H