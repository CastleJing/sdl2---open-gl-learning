#ifndef TRANSFORMATION_LEARNING_SHADER_H
#define TRANSFORMATION_LEARNING_SHADER_H

#include "IShaderFormFile.h"

class TransformationLearningShader : public gl::IShaderFormFile
{
public:
    TransformationLearningShader() :
        gl::IShaderFormFile("shader/TransformationLearningShader.vert",
                            "shader/TransformationLearningShader.frag",
                            "TransformationLearningShader")
                            { }
};


#endif //TRANSFORMATION_LEARNING_SHADER_H
