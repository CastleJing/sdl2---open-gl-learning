#include <cstdio>
#include <glad/glad.h>
#include <SDL2/SDL.h>

#include "shader/ShaderVAOLearningShader.h"

int main(int argc, char** argv)
{
    const int windowWidth = 600;
    const int windowHeight = 600;

    // 设置 SQL 版本和信息
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_Init(SDL_INIT_EVERYTHING);

    if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
    {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        return -1;
    }
    atexit(SDL_Quit);
    SDL_Window* window = SDL_CreateWindow("SDL 窗口", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowWidth, windowHeight, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
    if(window == nullptr)
    {
        printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
        return -1;
    }
    SDL_GL_CreateContext(window);
    SDL_GL_SetSwapInterval(1);
    //注册OpenGL函数指针
    if (!gladLoadGLLoader((GLADloadproc)SDL_GL_GetProcAddress))
    {
        printf("Failed to Load GL Loader for glad!");
        return -1;
    }


    ShaderVAOLearningShader shader;

    //创建顶点缓存对象
    uint32_t VBO;
    glGenBuffers(1, &VBO);
    //把顶点缓存对象绑定到GL_ARRAY_BUFF目标上
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    //创建顶点数组对象
    uint32_t VAO;
    glGenVertexArrays(1, &VAO);
    //绑定VAO
    glBindVertexArray(VAO);
    //声明三个顶点
    float vertices[] =
            {
                    0.626f, -0.35f, 0.0f,  1.0f, 0.0f, 0.0f,
                    -0.626f, -0.35f, 0.0f,  0.0f, 1.0f, 0.0f,
                    0.0f, 0.7f, 0.0f,  0.0f, 0.0f, 1.0f
            };
    //把顶点数据复制到缓冲内存上
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    //连接顶点属性
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)nullptr);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    //解绑VAO
    glBindVertexArray(0);
    //解绑VBO
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    uint32_t VBO2;
    glGenBuffers(1, &VBO2);
    glBindBuffer(GL_ARRAY_BUFFER, VBO2);
    uint32_t VAO2;
    glGenVertexArrays(1, &VAO2);
    glBindVertexArray(VAO2);
    float vertices2[] =
            {
                    -0.626f, 0.35f, 0.0f,  0.0f, 1.0f, 1.0f,
                    0.626f, 0.35f, 0.0f,  1.0f, 0.0f, 1.0f,
                    0.0f, -0.7f, 0.0f,  1.0f, 1.0f, 0.0f
            };
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices2), vertices2, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*) nullptr);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    uint32_t counter = 0;

    glEnable(GL_DEPTH_TEST);
    bool runFlag = true;
    SDL_Event event;

    //渲染循环
    while(runFlag)
    {
        //处理键盘事件
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
                runFlag = false;
        }

        glClearColor(0.22f, 0.33f, 0.33f, 0.5f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        counter++;
        counter %= 100;
        float value = (float)counter / 100.0f;

        shader.setFloat("d", value);
        shader.UseShader();
        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glBindVertexArray(VAO2);
        glDrawArrays(GL_TRIANGLES, 0, 3);

        SDL_GL_SwapWindow(window);
        SDL_Delay(33);
    }

    SDL_DestroyWindow(window);
    return 0;
}
