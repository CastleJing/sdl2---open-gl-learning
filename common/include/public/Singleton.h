#ifndef COMMON_SINGLETON
#define COMMON_SINGLETON

#include <memory>

template <typename T>
class Singleton
{
    inline static std::shared_ptr<T> instance = nullptr;

public:
    inline static T& Instance()
    {
        if (instance == nullptr)
            instance = std::make_shared<T>();
        return *instance;
    }
};

#endif //COMMON_SINGLETON
