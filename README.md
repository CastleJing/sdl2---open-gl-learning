# SDL2+OpenGL学习

#### 介绍
本项目是参照[OpenGL教程](https://learnopengl-cn.github.io/)所做的实现, 可以跨平台进行编译, 目前还在更新中, lesson8动画部分暂未更新完毕

#### 项目结构
* 本项目主项目下有多个子项目, 对应介绍中教程学习不同阶段的成果
* common子项目是其余项目依赖的公共代码
* thirdparty目录中存放了多个平台依赖的文件, 目前提供Windows下使用MinGW与MSVC编译运行所需要的SDL2以及Assimp的链接库与头文件, Linux与MacOS需要手动添加需要的库

#### 项目依赖
本项目依赖SDL2与Assimp运行
* Windows
本项目已经提供了在MinGW与MSVC下编译所需要的SDL2与Assimp的文件, 同时在生成项目时会自动将所需要动态库复制到目标目录, 无需手动解决依赖问题
* Ubuntu
在终端中使用以下代码安装所需要的依赖
```shell
sudo apt install libsdl2-dev
sudo apt install libassimp-dev
```

#### 编译
* Windows

    > * Windows下请使用CMake-Gui进行项目配置与生成(当然喜欢终端的话也可以使用CMake终端程序生成), 使用CMake-Gui可以很方便的生成VisualStudio工程(注意使用该工程时需要设置项目工作目录为二进制文件生成目录), 也可以生成MinGW项目. 
    > * Windows下也可以使用QtCreator更加简单的生成项目
    > * Windows下也可以使用以下命令行生成项目，"Visual Studio 16"为VisualStudio2019工程。其他参数请自行查询
    >     ```
    >     cmake -B build/win_2019 -G "Visual Studio 16"
    >     ```

* Linux
    > ```shell
    > mkdir build
    > cd build
    > cmake ..
    > make
    > ```