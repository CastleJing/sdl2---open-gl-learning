#ifndef COORDINATE_LEARNING_SHADER_H
#define COORDINATE_LEARNING_SHADER_H

#include "IShaderFormFile.h"

class CoordinateLearningShader : public gl::IShaderFormFile
{
public:
    CoordinateLearningShader() :
        gl::IShaderFormFile("shader/CoordinateLearningShader.vert",
                            "shader/CoordinateLearningShader.frag",
                            "CoordinateLearningShader")
                            { }
};


#endif //COORDINATE_LEARNING_SHADER_H
