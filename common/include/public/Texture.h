#ifndef COMMON_TEXTURE_H
#define COMMON_TEXTURE_H

#include <string>
#include <cstdint>

struct Texture
{
    std::string Uid;    //path or other string can uniquely represent a picture
    uint32_t TexId; //texture id used in OpenGL
    uint32_t Index; //index used in mesh
};

#endif //COMMON_TEXTURE_H
