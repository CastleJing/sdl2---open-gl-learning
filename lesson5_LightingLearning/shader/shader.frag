#version 330 core

in vec2 texPos;
in vec3 normal;
in vec3 fragPos;
out vec4 FragColor;
uniform sampler2D tex;
uniform float d;
uniform vec3 lightColor;
uniform vec3 lightPos;
uniform vec3 viewPos;
uniform bool isLight;
uniform float ambientStrength;
uniform float specularStrength;

void main()
{
	if(isLight)
	{
		FragColor = vec4(lightColor, 1.0f);
	}
	else
	{
		vec3 ambient = ambientStrength * lightColor;

		vec3 norm = normalize(normal);
		vec3 lightDir = normalize(lightPos - fragPos);
		float diff = max(dot(norm, lightDir), 0.0f);
		vec3 diffuse = diff * lightColor;

		vec3 viewDir = normalize(viewPos - fragPos);
		vec3 reflectDir = reflect(-lightDir, norm);
		float spec = pow(max(dot(viewDir, reflectDir), 0.0f), 32.0f);
		vec3 specular = specularStrength * spec * lightColor;

		FragColor = texture(tex, texPos) * vec4(ambient + diffuse + specular, 1.0f);
	}
}

