# Lesson8

#### 子项目描述
本子项目是[OpenGL教程](https://learnopengl-cn.github.io/)中模型加载章的实现, 并且本人擅自主张的加入了骨骼动画的相关实现, 动画插帧算法的实现学习了另一个[教程](https://ogldev.org/www/tutorial38/tutorial38.html), 并参考了其[源码](https://ogldev.org/ogldev-source.zip), 此外本子项目封装并不完善, 考虑之后专门开一个新的子项目来完善其封装.

#### 部分文件授权
本子项目的代码实现同样使用GPLv3开源, 本项目的资源文件并非开源, 仅供学习使用