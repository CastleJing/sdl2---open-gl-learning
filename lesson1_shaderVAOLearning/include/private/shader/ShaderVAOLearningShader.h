#ifndef SHADER_VAO_LEARNING_SHADER_H
#define SHADER_VAO_LEARNING_SHADER_H

#include "IShaderFormFile.h"

class ShaderVAOLearningShader : public gl::IShaderFormFile
{
public:
	ShaderVAOLearningShader() :
	    gl::IShaderFormFile("shader/ShaderVAOLearningShader.vert",
                            "shader/ShaderVAOLearningShader.frag",
                            "ShaderVAOLearningShader")
                            { }
};


#endif //SHADER_VAO_LEARNING_SHADER_H
