#version 330 core

layout (location = 0) in vec3 inWinPos;
layout (location = 1) in vec2 inTexPos;
out vec4 gl_Position;
out vec2 texPos;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;

void main()
{
	gl_Position = projMatrix * viewMatrix * modelMatrix * vec4(inWinPos.x, inWinPos.y, inWinPos.z, 1.0f);
	texPos = inTexPos;
}
