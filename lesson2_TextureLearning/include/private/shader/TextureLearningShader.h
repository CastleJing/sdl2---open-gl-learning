#ifndef TEXTURE_LEARNING_SHADER_H
#define TEXTURE_LEARNING_SHADER_H

#include "IShaderFormFile.h"

class TextureLearningShader : public gl::IShaderFormFile
{
public:
    TextureLearningShader() :
        gl::IShaderFormFile("shader/TextureLearningShader.vert",
                            "shader/TextureLearningShader.frag",
                            "TextureLearningShader")
                            { }
};


#endif //TEXTURE_LEARNING_SHADER_H
