#version 330 core

layout (location = 0) in vec3 inWinPos;
layout (location = 1) in vec2 inTexPos;
layout (location = 2) in vec3 inNormal;
out vec4 gl_Position;
out vec2 texPos;
out vec3 normal;
out vec3 fragPos;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;

uniform mat3 normalMatrix;

void main()
{
	vec4 fragP = modelMatrix * vec4(inWinPos.x, inWinPos.y, inWinPos.z, 1.0f);
	gl_Position = projMatrix * viewMatrix * fragP;
	fragPos = fragP.xyz;
	texPos = inTexPos;
	normal = normalMatrix * inNormal;
}
