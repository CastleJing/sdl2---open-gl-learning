#version 330 core

struct Material
{
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shininess;
};

struct Light {
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

in vec3 normal;
in vec3 fragPos;

out vec4 FragColor;

uniform bool isLight;
uniform vec3 lightColor;
uniform vec3 lightPos;
uniform vec3 viewPos;
uniform Material material;
uniform Light light;

void main()
{
	if(isLight)
	{
		FragColor = vec4(lightColor, 1.0f);
	}
	else
	{
		vec3 ambient = light.ambient * material.ambient;

		vec3 norm = normalize(normal);
		vec3 lightDir = normalize(lightPos - fragPos);
		float diffuseP = max(dot(norm, lightDir), 0.0f);
		vec3 diffuse = diffuseP * light.diffuse * material.diffuse;

		vec3 viewDir = normalize(viewPos - fragPos);
		vec3 reflectDir = reflect(-lightDir, norm);
		float specularP = pow(max(dot(viewDir, reflectDir), 0.0f), material.shininess);
		vec3 specular = specularP * light.specular * material.specular;

		FragColor = vec4(ambient + diffuse + specular, 1.0f);
	}
}

