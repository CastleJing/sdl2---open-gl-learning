#version 330 core

out vec4 FragColor;
uniform sampler2D tex;
uniform sampler2D tex2;
uniform float d;
in vec2 texPos;

void main()
{
	FragColor = mix(texture(tex, texPos), texture(tex2, texPos), d);
}

