#ifndef MODEL_ANIMATION_H
#define MODEL_ANIMATION_H

#include <glm/glm.hpp>
#include <cstdint>
#include <vector>
#include <iostream>
#include <exception>
#include <sstream>

class ModelAnimation
{
private:
    std::vector<std::shared_ptr<std::vector<glm::mat4>>> cache;
public:
    explicit ModelAnimation(std::vector<std::shared_ptr<std::vector<glm::mat4>>> frames) : cache(std::move(frames)){}
    [[nodiscard]] inline const glm::mat4& GetMatrix(uint32_t frameId, uint32_t boneId) const
    {
        if(frameId < cache.size())
        {
            auto& bones = cache[frameId];
            if(boneId < bones->size())
            {
                return (*bones)[boneId];
            }
        }
        std::stringstream ss;
        ss << "error find matrix [" << frameId << "][" << boneId << "]" << std::endl;
        throw std::out_of_range(ss.str());
    }
    [[nodiscard]] inline std::shared_ptr<std::vector<glm::mat4>> GetFrameAnim(uint32_t frameId)
    {
        if(frameId < cache.size())
            return cache[frameId];
        std::stringstream ss;
        ss << "error find matrix [" << frameId << "]" << std::endl;
        throw std::out_of_range(ss.str());
    }
    [[nodiscard]] inline uint32_t GetFrameSize() const
    {
        return cache.size();
    }
};

#endif //MODEL_ANIMATION_H
