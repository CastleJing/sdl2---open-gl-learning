#version 330 core

#define MAX_BONE_LENGTH 4
#define MAX_BONE_ANIM_LENGTH 128

layout (location = 0) in vec3 inWinPos;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec2 inTexPos;
layout (location = 3) in uint inBoneId[MAX_BONE_LENGTH];
// location should be 3 + MAX_BONE_LENGTH
layout (location = 7) in float inBoneWidget[MAX_BONE_LENGTH];

out vec4 gl_Position;
out vec2 texPos;
out vec3 normal;
out vec3 fragPos;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;

uniform mat4 boneAnimMatrix[MAX_BONE_ANIM_LENGTH];

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform sampler2D texture2;
uniform sampler2D texture3;

void main()
{
	mat4 animMatrix = mat4(0.0f);
	for (int i = 0; i < MAX_BONE_LENGTH; i++)
		animMatrix += boneAnimMatrix[inBoneId[i]] * inBoneWidget[i];
	if (inBoneWidget[0] == 0.0f)
		animMatrix = mat4(1.0f);

	mat4 mMatrix = modelMatrix * animMatrix;
	vec4 fragP = mMatrix * vec4(inWinPos, 1.0f);
	gl_Position = projMatrix * viewMatrix * fragP;
	fragPos = fragP.xyz;
	texPos = inTexPos;
	normal = mat3(transpose(inverse(mMatrix))) * inNormal;
}
