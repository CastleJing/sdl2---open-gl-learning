#ifndef MESH_H
#define MESH_H

#include <glm/glm.hpp>
#include <cstdint>
#include <string>
#include <vector>
#include <memory>
#include <IShader.h>
#include <Texture.h>

#define MAX_BONE_LENGTH 4
#define MAX_BONE_ANIM_LENGTH 128

struct Vertex
{
    glm::vec3 Position = {0, 0, 0};
    glm::vec3 Normal = {0, 0, 0};
    glm::vec2 TexCoords = {0, 0};
    uint32_t BoneId[MAX_BONE_LENGTH] = {0};
    float BoneWidget[MAX_BONE_LENGTH] = {0};
};

class Mesh
{
public:
    std::vector<Vertex> vertices;
    std::vector<uint32_t> indices;
    Texture texture;
    std::shared_ptr<std::vector<glm::mat4>> animVec;

    Mesh(const std::vector<Vertex>& vertices, const std::vector<uint32_t>& indices, const Texture& texture);
    void Draw(const gl::IShader& shader) const;
    void SetModelMatrix(const glm::mat4& mMat);
    void SetAnimVec(std::shared_ptr<std::vector<glm::mat4>> anim);

private:
    uint32_t VAO, VBO, EBO;
    glm::mat4 modelMatrix{};
    void SetupMesh();
};


#endif //MESH_H
