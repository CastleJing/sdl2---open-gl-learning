cmake_minimum_required(VERSION 3.10)

set(TARGET_NAME lesson2)
project(${TARGET_NAME})

file(COPY shader DESTINATION  .)
file(COPY bits DESTINATION  .)
file(COPY ${RUNTIME_FILE} DESTINATION .)

add_executable( ${TARGET_NAME}
        src/main.cpp)
target_include_directories( ${TARGET_NAME}
    PRIVATE
        include/private
)
target_link_libraries( ${TARGET_NAME}
    PRIVATE
        common
)
add_custom_command(
        TARGET ${TARGET_NAME}
        POST_BUILD
        COMMAND
        ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/shader $<TARGET_FILE_DIR:${TARGET_NAME}>/shader
)
add_custom_command(
        TARGET ${TARGET_NAME}
        POST_BUILD
        COMMAND
        ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/bits $<TARGET_FILE_DIR:${TARGET_NAME}>/bits
)
if (RUNTIME_FILE)
    add_custom_command(
            TARGET ${TARGET_NAME}
            POST_BUILD
            COMMAND
            ${CMAKE_COMMAND} -E copy_if_different ${RUNTIME_FILE} $<TARGET_FILE_DIR:${TARGET_NAME}>/
    )
endif()