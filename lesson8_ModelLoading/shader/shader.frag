#version 330 core

struct DirLight
{
	vec3 direction;
	vec3 color;
};

struct PointLight
{
	vec3 position;

	vec3 color;

	float constant;
	float linear;
	float quadratic;
};

struct SpotLight
{
	vec3 position;
	vec3 direction;

	vec3 color;

	float innerCutoff;
	float outerCutoff;
};

struct Material
{
	float ambientStrength;
	float diffuseStrength;
	float specularStrength;
	float shininess;
};

in vec2 texPos;
in vec3 normal;
in vec3 fragPos;

out vec4 FragColor;

uniform DirLight dirLight;
uniform PointLight pointLight;
uniform SpotLight spotLight;

uniform Material material;

uniform bool isPointLight;

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform sampler2D texture2;
uniform sampler2D texture3;

uniform vec3 viewPos;

vec3 DoDirLight(vec3 baseColor, DirLight light, vec3 normal, vec3 viewDir)
{
	vec3 lightDir = normalize(-light.direction);

	vec3 ambient = baseColor * light.color * material.ambientStrength;

	float diff = max(dot(normal, lightDir), 0.0f);
	vec3 diffuse = diff * baseColor * light.color * material.diffuseStrength;

	vec3 reflectDir = reflect(-lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0f), material.shininess);
	vec3 specular = spec * baseColor * light.color * material.specularStrength;

	return ambient + diffuse + specular;
}

vec3 DoPointLight(vec3 baseColor, PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	vec3 lightDir = normalize(light.position - fragPos);
	float distance = length(light.position - fragPos);
	float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * distance * distance);

	vec3 ambient = baseColor * light.color * material.ambientStrength;

	float diff = max(dot(lightDir, normal), 0.0f);
	vec3 diffuse = diff * baseColor * light.color * material.diffuseStrength;

	vec3 reflectDir = reflect(-lightDir, normal);
	float spec = pow(max(dot(reflectDir, viewDir), 0.0f), material.shininess);
	vec3 specular = spec * baseColor * light.color * material.specularStrength;

	return (ambient + diffuse + specular) * attenuation;
}

vec3 DoSpotLight(vec3 baseColor, SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	vec3 lightDir = normalize(light.position - fragPos);
	float theta = dot(lightDir, normalize(-light.direction));

	vec3 ambient = baseColor * light.color * material.ambientStrength;

	if (theta > light.outerCutoff)
	{
		float theta     = dot(lightDir, normalize(-light.direction));
		float epsilon   = light.innerCutoff - light.outerCutoff;
		float intensity = clamp((theta - light.outerCutoff) / epsilon, 0.0, 1.0);

		float diff = max(dot(lightDir, normal), 0.0f);
		vec3 diffuse = diff * baseColor * light.color * material.diffuseStrength;

		vec3 reflectDir = reflect(-lightDir, normal);
		float spec = pow(max(dot(reflectDir, viewDir), 0.0f), material.shininess);
		vec3 specular = spec * baseColor * light.color * material.specularStrength;

		return (diffuse + specular) * intensity + ambient;
	}

	return ambient;
}

void main()
{
	if (isPointLight)
	{
		FragColor = vec4(pointLight.color, 1.0f);
	}
	else
	{
		vec3 baseColor = texture(texture0, texPos).rgb;
		vec3 norm = normalize(normal);
		vec3 viewDir = normalize(viewPos - fragPos);

		float alpha = texture(texture0, texPos).a;

		vec3 dirLightColor = DoDirLight(baseColor, dirLight, norm, viewDir);
		vec3 pointLightColor = DoPointLight(baseColor, pointLight, norm, fragPos, viewDir);
		vec3 spotLightColor = DoSpotLight(baseColor, spotLight, norm, fragPos, viewDir);

		FragColor = vec4(dirLightColor + pointLightColor + spotLightColor, alpha);
	}
}

