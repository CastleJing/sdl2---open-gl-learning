#ifndef COMMON_INTERFACE_SHADER_FORM_FILE_H
#define COMMON_INTERFACE_SHADER_FORM_FILE_H

#include "IShader.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>

namespace gl
{
	class IShaderFormFile : public IShader
	{
	protected:
		IShaderFormFile(const char* vertexShaderFile, const char* fragmentShaderFile, const std::string& programName) : IShader(programName)
		{
			//打开文件
			std::ifstream vertexFileStream(vertexShaderFile);
			if(!vertexFileStream.is_open())
			{
				std::cerr << "Open vertex shader file failed!" << std::endl;
			}
			std::ifstream fragmentFileStream(fragmentShaderFile);
			if(!fragmentFileStream.is_open())
			{
				std::cerr << "Open fragment shader file failed!" << std::endl;
			}
			//加载着色器
			std::ostringstream tmp;
			tmp << vertexFileStream.rdbuf();
			LoadVertexShaderSource(tmp.str().c_str());
			vertexFileStream.close();
			tmp.str("");
			tmp << fragmentFileStream.rdbuf();
			LoadFragmentShaderSource(tmp.str().c_str());
			fragmentFileStream.close();
			tmp.clear();
			//编译连接并检查状态
			if(!CompileShaderAndLink())
			{
				std::cerr << GetErrorInfo() << std::endl;
			}
		};

	public:
		IShaderFormFile() = delete;
		IShaderFormFile(const IShaderFormFile& shaderFormFile) = delete;
	};
}


#endif //COMMON_INTERFACE_SHADER_FORM_FILE_H
