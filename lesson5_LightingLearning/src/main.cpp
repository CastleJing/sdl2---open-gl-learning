#include <cstdio>
#include <glad/glad.h>
#include <SDL2/SDL.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/euler_angles.hpp>
#define STB_IMAGE_IMPLEMENTATION
#include <stbImage/stb_image.h>
#include "shader/Shader.h"

int main(int argc, char** argv)
{
    const int windowWidth = 600;
    const int windowHeight = 600;
    // 设置 SQL 版本和信息
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_Init(SDL_INIT_EVERYTHING);

    if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
    {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        return -1;
    }
    atexit(SDL_Quit);
    SDL_Window* window = SDL_CreateWindow("SDL 窗口", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowWidth, windowHeight, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
    if(window == nullptr)
    {
        printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
        return -1;
    }
    SDL_GL_CreateContext(window);
    SDL_GL_SetSwapInterval(1);
    //注册OpenGL函数指针
    if (!gladLoadGLLoader((GLADloadproc)SDL_GL_GetProcAddress))
    {
        printf("Failed to Load GL Loader for glad!");
        return -1;
    }

    // 每个顶点由8个float组成, 由332分为三个属性, 分别为画布位置xyz, 颜色值rgb, 纹理采样位置st
    float vertices[] = {
            0.5f,  0.5f, 0.5f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f,   // 右上
            0.5f, -0.5f, 0.5f,  1.0f, 0.0f, 0.0f, 0.0f, 1.0f,   // 右下
            -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,   // 左下
            -0.5f,  0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,   // 左上

            0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f, 0.0f, -1.0f,   // 右上
            0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f, 0.0f, -1.0f,   // 右下
            -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f,   // 左下
            -0.5f,  0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f,   // 左上

            0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f, 1.0f, 0.0f,  // 右上
            0.5f,  0.5f, 0.5f,  1.0f, 0.0f, 0.0f, 1.0f, 0.0f,   // 右下
            -0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,    // 左下
            -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,   // 左上

            0.5f,  -0.5f, -0.5f,  1.0f, 1.0f, 0.0f, -1.0f, 0.0f,    // 右上
            0.5f,  -0.5f, 0.5f,  1.0f, 0.0f, 0.0f, -1.0f, 0.0f,     // 右下
            -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f,      // 左下
            -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.0f, -1.0f, 0.0f,     // 左上

            -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, -1.0f, 0.0f, 0.0f,    // 右上
            -0.5f,  0.5f, 0.5f,  1.0f, 0.0f, -1.0f, 0.0f, 0.0f,     // 右下
            -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,      // 左下
            -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f,     // 左上

            0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 1.0f, 0.0f, 0.0f,  // 右上
            0.5f,  0.5f, 0.5f,  1.0f, 0.0f, 1.0f, 0.0f, 0.0f,   // 右下
            0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,    // 左下
            0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f    // 左上
    };
    unsigned int indices[] = {
            0, 1, 3, // 第一个三角形
            1, 2, 3, // 第二个三角形

            4, 5, 7, // 第一个三角形
            5, 6, 7,  // 第二个三角形

            8, 9, 11, // 第一个三角形
            9, 10, 11,  // 第二个三角形

            12, 13, 15, // 第一个三角形
            13, 14, 15,  // 第二个三角形

            16, 17, 19, // 第一个三角形
            17, 18, 19,  // 第二个三角形

            20, 21, 23, // 第一个三角形
            21, 22, 23,  // 第二个三角形
    };

    glm::vec3 lightColor = glm::vec3(1.0f, 1.0f, 1.0f);
    glm::vec3 lightPos = glm::vec3(0.25f, -0.25f, 2.0f);

    glm::vec3 cubePos = glm::vec3(-0.5f, 0.5f, -2.0f);

    glm::vec3 camPos = glm::vec3(0.0f, 0.0f, 3.0f);
    glm::vec3 camRot = glm::vec3(0.0f, 0.0f, 0.0f);

    uint32_t VBO = 0;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    uint32_t VAO = 0;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)nullptr);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(5 * sizeof(float)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    uint32_t EBO = 0;
    glGenBuffers(1, &EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // 加载图形
    int width = 0, height = 0, channels = 0;
    uint8_t* data = stbi_load("bits/sa.png", &width, &height, &channels, 0);

    printf("image loaded.\nw: %d, h: %d, c: %d\n", width, height, channels);

    glActiveTexture(GL_TEXTURE0);
    // 创建纹理对象并绑定
    uint32_t textureId;
    glGenTextures(1, &textureId);
    glBindTexture(GL_TEXTURE_2D, textureId);
    // 设置纹理延伸方式
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    // 设置纹理采样方法
    // MIPMAP是多级渐远纹理采样方法
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // 创建纹理
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    // 创建多级渐远纹理
    glGenerateMipmap(GL_TEXTURE_2D);

    // 正交投影
//    glm::mat4 proj = glm::ortho(-(float)windowWidth / 600.0f,(float)windowWidth / 600.0f,
//                                 -(float)windowHeight / 600.0f,(float)windowHeight / 600.0f,
//                                 0.1f, 100.0f);
    // 透视投影
    glm::mat4 proj = glm::perspective(glm::radians(45.0f), (float)windowWidth / (float)windowHeight, 0.1f, 100.0f);

    // 释放图形内存
    stbi_image_free(data);


    Shader shader;
    int counter = 0;

    glEnable(GL_DEPTH_TEST);
    bool runFlag = true;
    SDL_Event event;

    glm::vec3 worldCamRot = glm::vec3(0.0f);

    //渲染循环
    while(runFlag)
    {
        //处理键盘事件
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
                runFlag = false;


            if (event.type == SDL_KEYDOWN)
            {
                if (event.key.keysym.sym == SDLK_i)
                    worldCamRot += glm::vec3(0.2f, 0.0f, 0.0f);
                if (event.key.keysym.sym == SDLK_k)
                    worldCamRot += glm::vec3(-0.2f, 0.0f, 0.0f);
                if (event.key.keysym.sym == SDLK_l)
                    worldCamRot += glm::vec3(0.0f, 0.2f, 0.0f);
                if (event.key.keysym.sym == SDLK_j)
                    worldCamRot += glm::vec3(0.0f, -0.2f, 0.0f);
                if (event.key.keysym.sym == SDLK_u)
                    worldCamRot += glm::vec3(0.0f, 0.0f, 0.2f);
                if (event.key.keysym.sym == SDLK_o)
                    worldCamRot += glm::vec3(0.0f, 0.0f, -0.2f);

                if (event.key.keysym.sym == SDLK_w)
                    camPos += glm::vec3(0.0f, 0.2f, 0.0f);
                if (event.key.keysym.sym == SDLK_s)
                    camPos += glm::vec3(0.0f, -0.2f, 0.0f);
                if (event.key.keysym.sym == SDLK_a)
                    camPos += glm::vec3(-0.2f, 0.0f, 0.0f);
                if (event.key.keysym.sym == SDLK_d)
                    camPos += glm::vec3(0.2f, 0.0f, 0.0f);
                if (event.key.keysym.sym == SDLK_q)
                    camPos += glm::vec3(0.0f, 0.0f, 0.2f);
                if (event.key.keysym.sym == SDLK_e)
                    camPos += glm::vec3(0.0f, 0.0f, -0.2f);

                if(event.key.keysym.sym == SDLK_SPACE)
                {
                    camPos = glm::vec3(0.0f, 0.0f, 3.0f);
                    worldCamRot = glm::vec3(0.0f, 0.0f, 0.0f);
                }

            }
        }

        glClearColor(0.22f, 0.33f, 0.33f, 0.5f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glm::mat4 view(1.0f);
        glm::mat4 worldCamRotM = glm::eulerAngleXYZ(worldCamRot.x, worldCamRot.y, worldCamRot.z);
        view = view * worldCamRotM;
        view = glm::translate(view, camPos);
        view = view * glm::eulerAngleXYZ(camRot.x, camRot.y, camRot.z);
        view = glm::inverse(view);

        shader.UseShader();
        shader.setMat4("projMatrix", proj);
        shader.setMat4("viewMatrix", view);
        shader.setVec3("lightColor", lightColor);
        shader.setVec3("lightPos", lightPos);
        shader.setVec3("viewPos", glm::mat3(worldCamRotM) * camPos);
        shader.setFloat("ambientStrength", 0.1f);
        shader.setFloat("specularStrength", 50.0f);

        auto d = (float)counter;
        counter = (counter + 5) % 359;

        {
            glm::mat4 model(1.0f);
            model = glm::translate(model, cubePos);
            model = glm::rotate(model, glm::radians(d + 20.0f), glm::vec3(0.5, 1.0, 0.5));
            model = glm::scale(model, glm::vec3(1.5f));
            shader.setBool("isLight", false);
            shader.setMat3("normalMatrix", glm::transpose(glm::inverse(glm::mat3(model))));
            shader.setMat4("modelMatrix", model);
            glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
        }

        {
            glm::mat4 model(1.0f);
            model = glm::translate(model, lightPos);
            model = glm::rotate(model, glm::radians(d + 20.0f), glm::vec3(0.5, 1.0, 0.5));
            model = glm::scale(model, glm::vec3(0.1f));
            shader.setBool("isLight", true);
            shader.setMat4("modelMatrix", model);
            glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
        }

        SDL_GL_SwapWindow(window);
        SDL_Delay(33);
    }

    SDL_DestroyWindow(window);
    return 0;
}
