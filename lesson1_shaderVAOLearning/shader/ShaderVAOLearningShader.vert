#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 pointColor;
uniform float d;
out vec3 inColor;
out vec4 gl_Position;

mat2 rot2mat2(float a)
{
	return mat2
	(
		cos(a), -1 * sin(a),
		sin(a), cos(a)
	);
}

mat3 rot2mat3(float x, float y, float z)
{
	mat3 rx = mat3
	(
		1, 0, 0,
		0, cos(x), -1 * sin(x),
		0, sin(x), cos(x)
	);

	mat3 ry = mat3
	(
		cos(y), 0, -1 * sin(y),
		0, 1, 0,
		sin(y), 0, cos(y)
	);

	mat3 rz = mat3
	(
		cos(z), -1 * sin(z), 0,
		sin(z), cos(z), 0,
		0, 0, 1
	);

	return rx * ry * rz;
}

void main()
{
	float a = 3.141592654 * 2 * d;
	mat2 rot = rot2mat2(a);
	vec2 newxy = aPos.xy * rot;
	gl_Position = vec4(newxy.x, newxy.y, aPos.z, 1.0f);
	inColor = pointColor;
}
